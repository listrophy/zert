//
//  DessertTests.swift
//  ZertTests
//
//  Created by Brad Grzesiak on 6/28/23.
//

import XCTest
@testable import Zert

final class DessertTests: XCTestCase {
    func testDecoding() throws {
        let json = """
            {
                "idMeal": "123",
                "strMeal": "Apple Pie",
                "strCategory": "Desserts",
                "strInstructions": "Make the Pie",
                "strMealThumb": "http://www.example.com/pie.png",
                "strTags": "American",
                "strYoutube": "http://www.example.com/youtube",
                "strSource": "http://www.example.com/cooking-website",
                "strIngredient1": "Flour",
                "strIngredient2": "Water",
                "strIngredient3": "The rest",
                "strIngredient4": "",
                "strMeasure1": "1 cup",
                "strMeasure2": "1 cup",
                "strMeasure3": " ",
                "strMeasure4": " "
            }
"""
        let jsonData = try XCTUnwrap(json.data(using: .utf8))
        let dessert = try decoder.decode(Dessert.self, from: jsonData)
        XCTAssertEqual(dessert.id, "123")
        XCTAssertEqual(dessert.name, "Apple Pie")
        XCTAssertEqual(dessert.instructions, "Make the Pie")
        try XCTAssertEqual(dessert.thumbnailURL, XCTUnwrap(URL(string: "http://www.example.com/pie.png")))
        XCTAssertEqual(dessert.ingredients[2].thing, "The rest")
        XCTAssertEqual(dessert.ingredients[2].amount, nil)
        XCTAssertEqual(dessert.ingredients.count, 3)
    }

    private let decoder = JSONDecoder()
}
