//
//  ZertApp.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import SwiftUI

@main
struct ZertApp: App {
    @StateObject var store = Store()

    var body: some Scene {
        WindowGroup {
            RootView()
                .environmentObject(store)
        }
    }
}
