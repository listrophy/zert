//
//  Store.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import Foundation

final class Store: ObservableObject {
    @Published var dessertLites: Result<[DessertLite], Error>?
    @Published var desserts = [String: Dessert]()

    init() {
        refreshDessertLites()
    }

    func refreshDessertLites() {
        Task {
            let result = try await self.fetcher.dessertLites()
            Task { @MainActor in
                self.dessertLites = .success(result)
            }
        }
    }

    func refreshDessert(id: String) {
        if desserts.keys.contains(id) {
            return
        }

        Task {
            let result = try await self.fetcher.dessert(id: id)
            Task { @MainActor in
                self.desserts[id] = result
            }
        }
    }

    private let fetcher = Fetcher()
}
