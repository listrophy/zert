//
//  DessertLite.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import Foundation

struct DessertLite: Identifiable {
    let id: String
    let name: String
    let thumbnailURL: URL
}

// MARK: - Decoding

extension DessertLite: Decodable {
    enum CodingKeys: String, CodingKey {
        case id = "idMeal"
        case name = "strMeal"
        case thumbnailURL = "strMealThumb"
    }
}

struct DessertLiteWrapper {
    let dessertLites: [DessertLite]
}

extension DessertLiteWrapper: Decodable {
    enum CodingKeys: String, CodingKey {
        case dessertLites = "meals"
    }
}
