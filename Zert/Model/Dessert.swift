//
//  Dessert.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import Foundation

struct Dessert: Identifiable {
    let id: String
    let name: String
    let thumbnailURL: URL
    let ingredients: [Ingredient]
    let instructions: String

    struct Ingredient: Identifiable {
        let id = UUID()
        let amount: String?
        let thing: String
    }

    static let empty: Dessert = .init(id: "",
                                      name: "",
                                      thumbnailURL: URL(string: "http://example.com")!,
                                      ingredients: [],
                                      instructions: "")
}

// MARK: - Decoding

extension Dessert: Decodable {
    /// The API provides JSON keys like "strInstructions". We prefer to convert that to just "instructions".
    ///
    /// Furthermore, the API provides a series of "strIngredient1", "strIngredient2", etc
    /// (with similar keys for "strMeasure1", etc). We prefer to collapse those into an
    /// `[Ingredient]` for easier display.
    private enum CodingKeys: CodingKey, Hashable {
        case id
        case name
        case instructions
        case thumbnailURL
        case ingredient(Int)
        case measure(Int)

        init?(stringValue: String) {
            if let value = Self.scalarLookup[stringValue] {
                self = value
            }
            else if let value = Self.parseIngredient(stringValue) {
                self = value
            }
            else if let value = Self.parseMeasure(stringValue) {
                self = value
            }
            else {
                return nil
            }
        }

        init?(intValue: Int) {
            return nil
        }

        var stringValue: String {
            switch self {
            case .ingredient(let index):
                return "\(Self.ingredientPrefix)\(index)"
            case .measure(let index):
                return "\(Self.measurePrefix)\(index)"
            case .id, .name, .instructions, .thumbnailURL:
                return Self.reverseScalarLookup[self] ?? ""
            }
        }

        var intValue: Int? { nil }

        static let scalarLookup = [
            "idMeal": Self.id,
            "strMeal": .name,
            "strInstructions": .instructions,
            "strMealThumb": .thumbnailURL
        ]

        static let reverseScalarLookup = [Self: String](uniqueKeysWithValues: scalarLookup.map({ key, value in
            (value, key)
        }))

        static let ingredientPrefix = "strIngredient"
        static let ingredientPrefixLength = ingredientPrefix.count
        static let measurePrefix = "strMeasure"
        static let measurePrefixLength = measurePrefix.count

        static func parseIngredient(_ string: String) -> Self? {
            guard string.starts(with: ingredientPrefix),
                  let index = Int(string.dropFirst(ingredientPrefixLength)) else {
                return nil
            }
            return .ingredient(index)
        }

        static func parseMeasure(_ string: String) -> Self? {
            guard string.starts(with: measurePrefix),
                  let index = Int(string.dropFirst(measurePrefixLength)) else {
                return nil
            }
            return .measure(index)
        }
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.id = try container.decode(String.self, forKey: .id)
        self.name = try container.decode(String.self, forKey: .name)
        self.thumbnailURL = try container.decode(URL.self, forKey: .thumbnailURL)
        self.instructions = try container.decode(String.self, forKey: .instructions)

        var extractedIngredients = [Int: String]()
        var extractedMeasurements = [Int: String]()
        for key in container.allKeys {
            switch key {
            case .ingredient(let index):
                try extractedIngredients[index] = container.decodeIfPresent(String.self, forKey: key)
            case .measure(let index):
                try extractedMeasurements[index] = container.decodeIfPresent(String.self, forKey: key)
            default:
                continue
            }
        }

        self.ingredients = extractedIngredients.keys.sorted().compactMap { ingredientIndex in
            guard let ingredient = extractedIngredients[ingredientIndex]?.strippingWhitespace,
                  !ingredient.isEmpty else {
                return nil
            }
            if let measure = extractedMeasurements[ingredientIndex]?.strippingWhitespace,
               !measure.isEmpty {
                return .init(amount: measure, thing: ingredient)
            }
            else {
                return .init(amount: nil, thing: ingredient)
            }
        }
    }
}

struct DessertWrapper {
    let desserts: [Dessert]
}

extension DessertWrapper: Decodable {
    enum CodingKeys: String, CodingKey {
        case desserts = "meals"
    }
}

extension StringProtocol {
    var strippingWhitespace: String {
        trimmingCharacters(in: .whitespaces)
    }
}
