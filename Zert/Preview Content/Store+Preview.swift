//
//  Store+Preview.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import Foundation

extension Store {
    static let previewable: Store = {
        var store = Store()
        store.dessertLites = .success([
            .init(id: "1", name: "Pie", thumbnailURL: URL(string: "https://example.com")!),
            .init(id: "2", name: "Cake", thumbnailURL: URL(string: "https://example.com")!)
        ])

        store.desserts = [
            "1": .init(id: "1",
                       name: "Pie",
                       thumbnailURL: URL(string: "https://placekitten.com/g/300/200")!,
                       ingredients: [.init(amount: "some", thing: "crust"),
                                     .init(amount: nil, thing: "filling")],
                       instructions: "Put it together"
                      ),
            "2": .init(id: "2",
                       name: "Cake",
                       thumbnailURL: URL(string: "https://placekitten.com/g/300/200")!,
                       ingredients: [.init(amount: nil, thing: "sponge"),
                                     .init(amount: "1 Cup", thing: "frosting")],
                       instructions: "Put it together"
                      )
        ]

        return store
    }()
}
