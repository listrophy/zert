//
//  RootView.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import SwiftUI

struct RootView: View {
    @State var sidebarVisible: NavigationSplitViewVisibility = .all
    @State var selectedID: String?

    var body: some View {
        NavigationSplitView(columnVisibility: $sidebarVisible) {
            DessertsView(selectedID: $selectedID)
        } detail: {
            if let selectedID {
                DessertView(selectedID: selectedID)
            }
            else {
                Text("No dessert selected")
            }
        }
        .navigationSplitViewStyle(.balanced)
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
