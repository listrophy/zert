//
//  DessertView.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import SwiftUI
import RegexBuilder

struct DessertView: View {
    @EnvironmentObject var store: Store
    let selectedID: String

    var body: some View {
        GeometryReader { geometry in
            if let dessert {
                ScrollView {
                    VStack(alignment: .leading) {
                        HeroImage(width: geometry.size.width,
                                  url: dessert.thumbnailURL)

                        Text("Ingredients")
                            .padding()
                            .font(.largeTitle)
                        IngredientsView(ingredients: dessert.ingredients)
                            .padding(.horizontal)

                        Text("Instructions")
                            .padding()
                            .font(.largeTitle)
                        InstructionsView(instructions: dessert.instructions)
                            .padding(.horizontal)
                    }
                }
                .navigationTitle(dessert.name)
            }
            else {
                VStack(alignment: .center) {
                    HStack(alignment: .center) {
                        ProgressView()
                        Text("Loading")
                    }
                }
                .frame(width: geometry.size.width,
                       height: geometry.size.height)
            }
        }
        .task(id: selectedID) {
            store.refreshDessert(id: selectedID)
        }
    }

    struct HeroImage: View {
        let width: CGFloat
        let url: URL

        var body: some View {
            AsyncImage(url: url) { image in
                image
                    .resizable()
                    .aspectRatio(contentMode: .fill)
                    .frame(width: width,
                           height: width * 9.0 / 16.0,
                           alignment: .center)
                    .clipped()
            } placeholder: {
                ProgressView()
            }
        }
    }

    struct IngredientsView: View {
        let ingredients: [Dessert.Ingredient]

        var body: some View {
            VStack(alignment: .ingredientAlignmentGuide, spacing: 4) {
                ForEach(ingredients) { ingredient in
                    HStack {
                        Text(ingredient.amount ?? "")
                            .alignmentGuide(.ingredientAlignmentGuide) { context in
                                context[.trailing]
                            }
                        Text(ingredient.thing)
                    }
                }
            }
        }

    }

    struct InstructionsView: View {
        let instructions: String
        var body: some View {
            Text(instructions.split(separator: .anyNonNewline.inverted).joined(separator: "\n\n"))
        }
    }

    private var dessert: Dessert? {
        store.desserts[selectedID]
    }
}

struct DessertView_Previews: PreviewProvider {
    static var previews: some View {
        DessertView(selectedID: "1")
            .environmentObject(Store.previewable)
    }
}

extension HorizontalAlignment {
    /// Custom alignment for ingredients
    private struct IngredientAlignment: AlignmentID {
        static func defaultValue(in context: ViewDimensions) -> CGFloat {
            // Default to trailing alignment if no guides are set.
            context[HorizontalAlignment.trailing]
        }
    }

    /// Guide for aligning ingredients
    static let ingredientAlignmentGuide = HorizontalAlignment(
        IngredientAlignment.self
    )
}
