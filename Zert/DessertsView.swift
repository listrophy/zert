//
//  DessertsView.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import SwiftUI

struct DessertsView: View {
    @EnvironmentObject var store: Store
    @Binding var selectedID: String?

    var body: some View {
        switch store.dessertLites {
        case .none:
            Text("Loading")
        case .failure(let error):
            Text("Error: \(error.localizedDescription)")
        case .success(let desserts):
            List(desserts, selection: $selectedID) { dessert in
                HStack {
                    AsyncImage(url: dessert.thumbnailURL) { image in
                        image.resizable()
                    } placeholder: {
                        ProgressView()
                    }
                    .frame(width: 44, height: 44)

                    Text(dessert.name)
                    Spacer()
                }
            }
            .listStyle(.plain)
            .navigationTitle("Zerts")
        }
    }
}

struct DessertsView_Previews: PreviewProvider {
    static var previews: some View {
        DessertsView(selectedID: .constant(nil))
            .environmentObject(Store.previewable)
    }
}
