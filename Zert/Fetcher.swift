//
//  Fetcher.swift
//  Zert
//
//  Created by Brad Grzesiak on 6/28/23.
//

import Foundation

final class Fetcher {
    func dessertLites() async throws -> [DessertLite] {
        let urlRequest = URLRequest(url: dessertLitesURL)
        let (data, _) = try await urlSession.data(for: urlRequest)
        let wrapper = try decoder.decode(DessertLiteWrapper.self, from: data)

        return wrapper.dessertLites
    }

    func dessert(id: String) async throws -> Dessert {
        guard let url = dessertURL(id: id) else {
            throw EncodingError.invalidValue(id, .init(codingPath: [], debugDescription: "invalid ID"))
        }
        let urlRequest = URLRequest(url: url)
        let (data, _) = try await urlSession.data(for: urlRequest)
        let wrapper = try decoder.decode(DessertWrapper.self, from: data)

        guard let dessert = wrapper.desserts.first else {
            throw DecodingError.valueNotFound(DessertWrapper.self,
                                              .init(codingPath: [DessertWrapper.CodingKeys.desserts],
                                                    debugDescription: "first dessert not found"))
        }
        return dessert

    }

    private let urlSession = URLSession(configuration: .default)

    private let decoder = JSONDecoder()
    private let dessertLitesURL = URL(string: "https://www.themealdb.com/api/json/v1/1/filter.php?c=Dessert")!

    private func dessertURL(id: String) -> URL? {
        URL(string: "https://themealdb.com/api/json/v1/1/lookup.php?i=\(id)")
    }
}
