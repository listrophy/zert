# Zert App

Dessert browsing app—based on themealdb.com API—for consideration of a role at Fetch.

## Features

* SwiftUI
* iPhone and iPad
* Fully typesafe JSON decoding, making no assumptions about the limit of ingredients (despite evidence implying it doesn't go beyond 20)
